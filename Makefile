V=52.9.20210602

srcver:
	./generatesrcver

dist:
	./makesignxpifiles

upload:
	rsync -av -e 'ssh -p 51000' --progress $(V) repo@dusseldorf.hyperbola.info:/srv/repo/other/icedove-uxp/l10n

.PHONY: srcver dist upload
