# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (connection.*)
#   These will be displayed in the account manager in order to show the progress
#   of the connection.
#   (These will be displayed in account.connection.progress from
#    accounts.properties, which adds … at the end, so do not include
#    periods at the end of these messages.)
connection.initializingStream=Menginisialisasi aliran
connection.initializingEncryption=Menginisialisasi enkripsi
connection.authenticating=Mengautentikasi
connection.gettingResource=Mengambil sumber daya
connection.downloadingRoster=Mengunduh daftar kontak

# LOCALIZATION NOTE (connection.error.*)
#   These will show in the account manager if an error occurs during the
#   connection attempt.
connection.error.invalidUsername=Nama pengguna salah (nama pengguna harus mengandung sebuah karakter '@')
connection.error.failedToCreateASocket=Gagal membuat soket (Apakah sedang luring?)
connection.error.serverClosedConnection=Server menutup sambungan
connection.error.resetByPeer=Sambungan disetel ulang oleh mitra
connection.error.timedOut=Tenggan waktu tersambung habis
connection.error.receivedUnexpectedData=Data tak diharapkan diterima
connection.error.incorrectResponse=Balasan salah diterima
connection.error.startTLSRequired=Server mewajibkan enkripsi tetapi Anda telah menonaktifkannya
connection.error.startTLSNotSupported=Server tidak mendukung enkripsi tetapi konfigurasi Anda mewajibkannya
connection.error.failedToStartTLS=Gagal memulai enkripsi
connection.error.noAuthMec=Tidak ada mekanisme autentikasi yang ditawarkan server
connection.error.noCompatibleAuthMec=Tidak ada mekanisme autentikasi yang ditawarkan server yang dapat didukung
connection.error.notSendingPasswordInClear=Server hanya mendukung autentikasi dengan cara mengirimkan sandi dalam bentuk teks polos
connection.error.authenticationFailure=Kegagalan autentikasi
connection.error.notAuthorized=Tidak diizinkan (Sandinya sudah benar?)
connection.error.failedToGetAResource=Gagal mengambil sumber daya

# LOCALIZATION NOTE (conversation.error.notDelivered):
#   This is displayed in a conversation as an error message when a message
#   the user has sent wasn't delivered.
#   %S is replaced by the text of the message that wasn't delivered.
conversation.error.notDelivered=Pesan ini tidak dapat dikirim: %S
#   This is displayed in a conversation as an error message when joining a MUC
#   fails.
#   %S is the name of the MUC.
conversation.error.joinFailed=Tidak dapat bergabung: %S
#   This is displayed in a conversation as an error message when the user is
#   banned from a room.
#   %S is the name of the MUC room.
conversation.error.joinForbidden=Tidak dapat bergabung dengan %S karena Anda diblokir dari kamar ini.
conversation.error.joinFailedNotAuthorized=Registrasi diperlukan: Anda tidak berwenang untuk bergabung dengan kamar ini.
conversation.error.creationFailedNotAllowed=Akses dibatasi: Anda tidak diizinkan membuat kamar.
#   This is displayed in a conversation as an error message when remote server
#   is not found.
#   %S is the name of MUC room.
#   This is displayed in a conversation as an error message when the user sends
#   a message to a room that he is not in.
#   %1$S is the name of MUC room.
#   %2$S is the text of the message that wasn't delivered.
#   This is displayed in a conversation as an error message when the user sends
#   a message to a room that the recipient is not in.
#   %1$S is the jid of the recipient.
#   %2$S is the text of the message that wasn't delivered.
#   These are displayed in a conversation as a system error message.
#   %S is the name of the message recipient.
#   %S is the nick of participant that is not in room.
#   %S is the jid of user that is invited.
#   %S is the jid that is invalid.
#   %S is the name of the recipient.

# LOCALIZATION NOTE (conversation.error.version.*):
#   %S is the name of the recipient.

# LOCALIZATION NOTE (tooltip.*):
#   These are the titles of lines of information that will appear in
#   the tooltip showing details about a contact or conversation.
# LOCALIZATION NOTE (tooltip.status):
#   %S will be replaced by the XMPP resource identifier
tooltip.status=Status (%S)
tooltip.statusNoResource=Status
tooltip.subscription=Langganan

# LOCALIZATION NOTE (chatRoomField.*):
#   These are the name of fields displayed in the 'Join Chat' dialog
#   for XMPP accounts.
#   The _ character won't be displayed; it indicates the next
#   character of the string should be used as the access key for this
#   field.
chatRoomField.room=_Kamar
chatRoomField.server=_Server
chatRoomField.nick=_Alias
chatRoomField.password=_Sandi

# LOCALIZATION NOTE (conversation.muc.*):
#   These are displayed as a system message when a chatroom invitation is
#   received.
#   %1$S is the inviter.
#   %2$S is the room.
#   %3$S is the reason which is a message provided by the person sending the
#   invitation.
#   %3$S is the password of the room.
#   %4$S is the reason which is a message provided by the person sending the
#   invitation.
#   %3$S is the password of the room.

# LOCALIZATION NOTE (conversation.muc.join):
#   This is displayed as a system message when a participant joins room.
#   %S is the nick of the participant.

# LOCALIZATION NOTE (conversation.muc.rejoined):
#   This is displayed as a system message when a participant rejoins room after
#   parting it.

# LOCALIZATION NOTE (conversation.message.parted.*):
#   These are displayed as a system message when a participant parts a room.
#   %S is the part message supplied by the user.
#   %1$S is the participant that is leaving.
#   %2$S is the part message supplied by the participant.

# LOCALIZATION NOTE (conversation.message.invitationDeclined*):
#   %1$S is the invitee that declined the invitation.
#   %2$S is the decline message supplied by the invitee.

# LOCALIZATION NOTE (conversation.message.banned.*):
#   These are displayed as a system message when a participant is banned from
#   a room.
#   %1$S is the participant that is banned.
#   %2$S is the reason.
#   %3$S is the person who is banning.
#   %1$S is the person who is banning.
#   %2$S is the participant that is banned.
#   %3$S is the reason.
#   %1$S is the reason.
#   %1$S is the person who is banning.
#   %2$S is the reason.

# LOCALIZATION NOTE (conversation.message.kicked.*):
#   These are displayed as a system message when a participant is kicked from
#   a room.
#   %1$S is the participant that is kicked.
#   %2$S is the reason.
#   %1$S is the person who is kicking.
#   %2$S is the participant that is kicked.
#   %3$S is the reason.
#   %1$S is the reason.
#   %1$S is the person who is kicking.
#   %2$S is the reason.

# LOCALIZATION NOTE (conversation.message.removedNonMember.*):
#   These are displayed as a system message when a participant is removed from
#   a room because the room has been changed to members-only.
#   %1$S is the participant that is removed.
#   %2$S is the person who changed the room configuration.
#   %1$S is the person who changed the room configuration.

# LOCALIZATION NOTE (conversation.message.MUCShutdown):
#   These are displayed as a system message when a participant is removed from
#   a room because of a system shutdown.

# LOCALIZATION NOTE (conversation.message.version*):
#   %1$S is the name of the user whose version was requested.
#   %2$S is the client name response from the client.
#   %3$S is the client version response from the client.
#   %4$S is the operating system(OS) response from the client.

# LOCALIZATION NOTE (options.*):
#   These are the protocol specific options shown in the account manager and
#   account wizard windows.
options.resource=Sumber daya
options.priority=Prioritas
options.connectionSecurity=Keamanan Sambungan
options.connectionSecurity.requireEncryption=Enkripsi wajib
options.connectionSecurity.opportunisticTLS=Gunakan enkripsi jika tersedia
options.connectionSecurity.allowUnencryptedAuth=Izinkan mengirimkan sandi tanpa enkripsi
options.connectServer=Server
options.connectPort=Port

# LOCALIZATION NOTE (*.protocolName)
#  This name is used whenever the name of the protocol is shown.

# LOCALIZATION NOTE (gtalk.usernameHint):
#  This is displayed inside the accountUsernameInfoWithDescription
#  string defined in imAccounts.properties when the user is
#  configuring a Google Talk account.
gtalk.usernameHint=alamat email

# LOCALIZATION NOTE (odnoklassniki.usernameHint):
#  This is displayed inside the accountUsernameInfoWithDescription
#  string defined in imAccounts.properties when the user is
#  configuring a Odnoklassniki account.

# LOCALZIATION NOTE (command.*):
#  These are the help messages for each command.
command.part2=%S [&lt;message&gt;]: Meninggalkan kamar saat ini dengan pesan opsional.

command.ban=%S &lt;nick&gt;[&lt;message&gt;]: Ban someone from the room. You must be a room administrator to do this.
command.invite=%S &lt;jid&gt;[&lt;message&gt;]: Invite a user to join the current room with an optional message.
command.inviteto=%S &lt;room jid&gt;[&lt;password&gt;]: Invite your conversation partner to join a room, together with its password if required.

# LOCALZIATION NOTE (command.*):
#  These are the help messages for each command.
command.join3=%S [&lt;room&gt;[@&lt;server&gt;][/&lt;nick&gt;]] [&lt;password&gt;]: Join a room, optionally providing a different server, or nickname, or the room password.
command.kick=%S &lt;nick&gt;[&lt;message&gt;]: Remove someone from the room. You must be a room moderator to do this.
command.me=%S &lt;action to perform&gt;: Perform an action.
command.msg=%S &lt;nick&gt; &lt;message&gt;: Send a private message to a participant in the room.
command.nick=%S &lt;new nickname&gt;: Change your nickname.
command.topic=%S [&lt;new topic&gt;]: Set this room's topic.
command.version=%S: Request information about the client your conversation partner is using.
connection.error.XMPPNotSupported=This server does not support XMPP
connection.error.failedMaxResourceLimit=This account is connected from too many places at the same time.
connection.error.failedResourceNotValid=Resource is not valid.
connection.srvLookup=Looking up the SRV record
conversation.error.banCommandAnonymousRoom=You can't ban participants from anonymous rooms. Try /kick instead.
conversation.error.banKickCommandConflict=Sorry, you can't remove yourself from the room.
conversation.error.banKickCommandNotAllowed=You don't have the required privileges to remove this participant from the room.
conversation.error.changeNickFailedConflict=Could not change your nick to %S as this nick is already in use.
conversation.error.changeNickFailedNotAcceptable=Could not change your nick to %S as nicks are locked down in this room.
conversation.error.changeTopicFailedNotAuthorized=You are not authorized to set the topic of this room.
conversation.error.commandFailedNotInRoom=You have to rejoin the room to be able to use this command.
#   %S is the jid of user that is invited.
conversation.error.failedJIDNotFound=Could not reach %S.
#   %S is the jid that is invalid.
conversation.error.invalidJID=%S is an invalid jid (Jabber identifiers must be of the form user@domain).
conversation.error.inviteFailedForbidden=You don't have the required privileges to invite users to this room.
#   This is displayed in a conversation as an error message when remote server
#   is not found.
#   %S is the name of MUC room.
conversation.error.joinFailedRemoteServerNotFound=Could not join the room %S as the server the room is hosted on could not be reached.
#   %S is the nick of participant that is not in room.
conversation.error.nickNotInRoom=%S is not in the room.
#   These are displayed in a conversation as a system error message.
conversation.error.remoteServerNotFound=Could not reach the recipient's server.
#   %S is the name of the recipient.
conversation.error.resourceNotAvailable=You must talk first as %S could be connected with more than one client.
#   This is displayed in a conversation as an error message when the user sends
#   a message to a room that he is not in.
#   %1$S is the name of MUC room.
#   %2$S is the text of the message that wasn't delivered.
conversation.error.sendFailedAsNotInRoom=Message could not be sent to %1$S as you are no longer in the room: %2$S
#   This is displayed in a conversation as an error message when the user sends
#   a message to a room that the recipient is not in.
#   %1$S is the jid of the recipient.
#   %2$S is the text of the message that wasn't delivered.
conversation.error.sendFailedAsRecipientNotInRoom=Message could not be sent to %1$S as the recipient is no longer in the room: %2$S
#   %S is the name of the message recipient.
conversation.error.sendServiceUnavailable=It is not possible to send messages to %S at this time.
conversation.error.unknownSendError=An unknown error occurred on sending this message.

# LOCALIZATION NOTE (conversation.error.version.*):
#   %S is the name of the recipient.
conversation.error.version.unknown=%S's client does not support querying for its software version.

# LOCALIZATION NOTE (conversation.message.banned.*):
#   These are displayed as a system message when a participant is banned from
#   a room.
#   %1$S is the participant that is banned.
#   %2$S is the reason.
#   %3$S is the person who is banning.
conversation.message.banned=%1$S has been banned from the room.
#   %1$S is the person who is banning.
#   %2$S is the participant that is banned.
#   %3$S is the reason.
conversation.message.banned.actor=%1$S has banned %2$S from the room.
conversation.message.banned.actor.reason=%1$S has banned %2$S from the room: %3$S
conversation.message.banned.reason=%1$S has been banned from the room: %2$S
conversation.message.banned.you=You have been banned from the room.
#   %1$S is the person who is banning.
#   %2$S is the reason.
conversation.message.banned.you.actor=%1$S has banned you from the room.
conversation.message.banned.you.actor.reason=%1$S has banned you from the room: %2$S
#   %1$S is the reason.
conversation.message.banned.you.reason=You have been banned from the room: %1$S

# LOCALIZATION NOTE (conversation.message.invitationDeclined*):
#   %1$S is the invitee that declined the invitation.
#   %2$S is the decline message supplied by the invitee.
conversation.message.invitationDeclined=%1$S has declined your invitation.
conversation.message.invitationDeclined.reason=%1$S has declined your invitation: %2$S

# LOCALIZATION NOTE (conversation.muc.join):
#   This is displayed as a system message when a participant joins room.
#   %S is the nick of the participant.
conversation.message.join=%S entered the room.

# LOCALIZATION NOTE (conversation.message.kicked.*):
#   These are displayed as a system message when a participant is kicked from
#   a room.
#   %1$S is the participant that is kicked.
#   %2$S is the reason.
conversation.message.kicked=%1$S has been kicked from the room.
#   %1$S is the person who is kicking.
#   %2$S is the participant that is kicked.
#   %3$S is the reason.
conversation.message.kicked.actor=%1$S has kicked %2$S from the room.
conversation.message.kicked.actor.reason=%1$S has kicked %2$S from the room: %3$S
conversation.message.kicked.reason=%1$S has been kicked from the room: %2$S
conversation.message.kicked.you=You have been kicked from the room.
#   %1$S is the person who is kicking.
#   %2$S is the reason.
conversation.message.kicked.you.actor=%1$S has kicked you from the room.
conversation.message.kicked.you.actor.reason=%1$S has kicked you from the room: %2$S
#   %1$S is the reason.
conversation.message.kicked.you.reason=You have been kicked from the room: %1$S

# LOCALIZATION NOTE (conversation.message.MUCShutdown):
#   These are displayed as a system message when a participant is removed from
#   a room because of a system shutdown.
conversation.message.mucShutdown=You have been removed from the room because of a system shutdown.
#   %1$S is the participant that is leaving.
#   %2$S is the part message supplied by the participant.
conversation.message.parted=%1$S has left the room.
conversation.message.parted.reason=%1$S has left the room: %2$S

# LOCALIZATION NOTE (conversation.message.parted.*):
#   These are displayed as a system message when a participant parts a room.
#   %S is the part message supplied by the user.
conversation.message.parted.you=You have left the room.
conversation.message.parted.you.reason=You have left the room: %S

# LOCALIZATION NOTE (conversation.muc.rejoined):
#   This is displayed as a system message when a participant rejoins room after
#   parting it.
conversation.message.rejoined=You have rejoined the room.

# LOCALIZATION NOTE (conversation.message.removedNonMember.*):
#   These are displayed as a system message when a participant is removed from
#   a room because the room has been changed to members-only.
#   %1$S is the participant that is removed.
#   %2$S is the person who changed the room configuration.
conversation.message.removedNonMember=%1$S has been removed from the room because its configuration was changed to members-only.
conversation.message.removedNonMember.actor=%1$S has been removed from the room because %2$S has changed it to members-only.
conversation.message.removedNonMember.you=You have been removed from the room because its configuration has been changed to members-only.
#   %1$S is the person who changed the room configuration.
conversation.message.removedNonMember.you.actor=You have been removed from the room because %1$S has changed it to members-only.

# LOCALIZATION NOTE (conversation.message.version*):
#   %1$S is the name of the user whose version was requested.
#   %2$S is the client name response from the client.
#   %3$S is the client version response from the client.
#   %4$S is the operating system(OS) response from the client.
conversation.message.version=%1$S is using "%2$S %3$S".
conversation.message.versionWithOS=%1$S is using "%2$S %3$S" on %4$S.

# LOCALIZATION NOTE (conversation.muc.*):
#   These are displayed as a system message when a chatroom invitation is
#   received.
#   %1$S is the inviter.
#   %2$S is the room.
#   %3$S is the reason which is a message provided by the person sending the
#   invitation.
conversation.muc.invitationWithReason2=%1$S has invited you to join %2$S: %3$S
#   %3$S is the password of the room.
#   %4$S is the reason which is a message provided by the person sending the
#   invitation.
conversation.muc.invitationWithReason2.password=%1$S has invited you to join %2$S with password %3$S: %4$S
conversation.muc.invitationWithoutReason=%1$S has invited you to join %2$S
#   %3$S is the password of the room.
conversation.muc.invitationWithoutReason.password=%1$S has invited you to join %2$S with password %3$S

# LOCALIZATION NOTE (*.protocolName)
#  This name is used whenever the name of the protocol is shown.
gtalk.protocolName=Google Talk
odnoklassniki.protocolName=Odnoklassniki

# LOCALIZATION NOTE (odnoklassniki.usernameHint):
#  This is displayed inside the accountUsernameInfoWithDescription
#  string defined in imAccounts.properties when the user is
#  configuring a Odnoklassniki account.
odnoklassniki.usernameHint=Profile ID
options.domain=Domain
tooltip.birthday=Birthday
tooltip.country=Country
tooltip.email=Email
tooltip.fullName=Full Name
tooltip.locality=Locality
tooltip.nickname=Nickname
tooltip.organization=Organization
tooltip.title=Title
tooltip.userName=Username
